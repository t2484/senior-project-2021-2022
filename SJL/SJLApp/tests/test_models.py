from django.test import TestCase
from django.contrib.auth.models import User
from ..models import Shader

class TestProfile(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='username', password='password')

    def test_profile_exists(self):
        self.assertIsNotNone(self.user.profile)

    def test_profile_about(self):
        self.user.profile.about = "About Me"
        self.user.save()
        self.assertTrue(self.user.profile.about, "About Me")

class TestShader(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='username', password='password')

    def test_shader_default(self):
        shader = Shader()
        shader.user = self.user
        shader.save()
        self.assertEqual(shader.title, Shader._meta.get_field('title').get_default())
        self.assertEqual(shader.description, Shader._meta.get_field('description').get_default())
        self.assertEqual(shader.is_public, Shader._meta.get_field('is_public').get_default())
        self.assertEqual(shader.user, self.user)
        self.assertEqual(shader.config, Shader._meta.get_field('config').get_default())

    def test_shader_comments(self):
        shader = Shader()
        shader.user = self.user
        shader.save()
        comment = shader.comment_set.create(pk=1, user=self.user)
        self.assertEqual(shader.comment_set.get(pk=1), comment)
