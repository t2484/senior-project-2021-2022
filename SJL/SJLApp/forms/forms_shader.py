from django import forms
from SJLApp.models import Shader, Comment

class ShaderForm(forms.ModelForm):
    class Meta:
        model = Shader
        fields = ['title', 'description','code', 'is_public', 'config']
        widgets = {
            'title': forms.TextInput(attrs={
                'id': 'shader-title',
                'placeholder': 'Default Title', 
                'class': 'form-control',
            }),
            'description': forms.Textarea(attrs={
                'id': 'shader-description',
                'placeholder': 'Default Description', 
                'class': 'form-control',
                'type': 'textarea',
                'rows': '5',
                'style': 'height: 100%;'
            }),
            'is_public': forms.CheckboxInput(attrs={ 
                'id': 'shader-ispublic',
                'class': 'form-check-input btn-danger bg-danger border-danger',
                'type': 'checkbox',
            }),
            'code' : forms.Textarea(attrs={
                'id': 'shader-code',
                'hidden': 'hidden',
            }),
            'config' : forms.Textarea(attrs={
                'id': 'shader-config',
                'hidden': 'hidden',
            }),
        }

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['content']
        widgets = {
            'content': forms.Textarea(attrs={
                'placeholder': 'Default Comment', 
                'class': 'form-control',
            }),
        }