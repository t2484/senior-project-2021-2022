from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.auth import get_user
from django.urls import reverse
from SJLApp.views import *
from SJLApp.models import *

class TestAboutUs(TestCase):
    def test_about_us(self):
        response = self.client.get(reverse('SJLApp:about_us'))
        self.assertEqual(response.status_code, 200)

class TestUser(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()

    def test_user_authenticated(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('SJLApp:user'))
        self.assertEqual(response.status_code, 200)

    def test_user_authenticated_query(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('SJLApp:user')+"?pk=1")
        self.assertEqual(response.status_code, 200)

    def test_user_unauthenticated(self):
        response = self.client.get(reverse('SJLApp:user'))
        self.assertEqual(response.status_code, 302)

class TestAnyUser(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)

    def test_any_user(self):
        response = self.client.get(reverse('SJLApp:user_detail_page', kwargs={'pk': self.user.id}))
        self.assertEqual(response.status_code, 200)

    def test_any_user_query(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('SJLApp:user_detail_page', kwargs={'pk': self.user.id})+"?pk=1")
        self.assertEqual(response.status_code, 200)

    def test_any_user_authenticated_bad(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('SJLApp:user_detail_page', kwargs={'pk': 404}))
        self.assertEqual(response.status_code, 302)

    def test_any_user_unauthenticated_bad(self):
        response = self.client.get(reverse('SJLApp:user_detail_page', kwargs={'pk': 404}))
        self.assertEqual(response.status_code, 404)

class TestIndex(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()

    def test_index(self):
        response = self.client.get(reverse('SJLApp:index'))
        self.assertEqual(response.status_code, 200)

    def test_index_query(self):
        response = self.client.get(reverse('SJLApp:index')+"?pk=1")
        self.assertEqual(response.status_code, 200)

class TestShaderDetail(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()

    def test_shader_detail_owner(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
        self.assertEqual(response.status_code, 200)

    def test_shader_detail_viewer(self):
        response = self.client.get(reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
        self.assertEqual(response.status_code, 200)

    def test_shader_detail_not_public(self):
        self.shader.is_public = False
        self.shader.save()
        response = self.client.get(reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
        self.assertEqual(response.status_code, 302)

    def test_shader_detail_bad(self):
        response = self.client.get(reverse('SJLApp:shader_detail_page', kwargs={'pk': 404}))
        self.assertEqual(response.status_code, 302)

class TestCreateUserPage(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
    
    def test_create_user_page(self):
        response = self.client.get(reverse('SJLApp:create_user_page'))
        self.assertEqual(response.status_code, 200)

    def test_create_user_page_authenticated(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('SJLApp:create_user_page'))
        self.assertEqual(response.status_code, 302)

class TestUserLoginPage(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
    
    def test_create_user_page(self):
        response = self.client.get(reverse('SJLApp:login_page'))
        self.assertEqual(response.status_code, 200)

    def test_create_user_page_authenticated(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('SJLApp:login_page'))
        self.assertEqual(response.status_code, 302)

class TestShaderApiIndex(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()

    def test_shader_api_index(self):
        response = self.client.get(reverse('SJLApp:api_shaders'))
        self.assertEqual(response.status_code, 200)

    def test_shader_api_index_query(self):
        response = self.client.get(reverse('SJLApp:api_shaders')+"?page=1")
        self.assertEqual(response.status_code, 200)

    def test_shader_api_index_bad_query(self):
        response = self.client.get(reverse('SJLApp:api_shaders')+"?adbskksab=1")
        self.assertEqual(response.status_code, 302)

class TestShaderApiDetail(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()

    def test_shader_api_detail(self):
        response = self.client.get(reverse('SJLApp:api_shader', kwargs={'pk': self.shader.id}))
        self.assertEqual(response.status_code, 200)

    def test_shader_api_detail_not_public(self):
        self.shader.is_public = False
        self.shader.save()
        response = self.client.get(reverse('SJLApp:api_shader', kwargs={'pk': self.shader.id}))
        self.assertEqual(response.status_code, 302)

    def test_shader_api_detail_bad(self):
        response = self.client.get(reverse('SJLApp:api_shader', kwargs={'pk': 404}))
        self.assertEqual(response.status_code, 404)

class TestShaderApiUserIndex(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()

    def test_shader_api_user_index(self):
        response = self.client.get(reverse('SJLApp:api_user_shaders', kwargs={'pk': self.shader.id}))
        self.assertEqual(response.status_code, 200)

    def test_shader_api_user_index_authenticated(self):
        self.client.login(**self.credentials)
        response = self.client.get(reverse('SJLApp:api_user_shaders', kwargs={'pk': self.shader.id}))
        self.assertEqual(response.status_code, 200)

    def test_shader_api_user_index_query(self):
        response = self.client.get(reverse('SJLApp:api_user_shaders', kwargs={'pk': self.shader.id})+"?page=1")
        self.assertEqual(response.status_code, 200)

    def test_shader_api_user_index_bad_query(self):
        response = self.client.get(reverse('SJLApp:api_user_shaders', kwargs={'pk': self.shader.id})+"?djaskdnj=1")
        self.assertEqual(response.status_code, 302)

    def test_shader_api_user_detail_bad(self):
        response = self.client.get(reverse('SJLApp:api_user_shaders', kwargs={'pk': 404}))
        self.assertEqual(response.status_code, 404)