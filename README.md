# ![Shady Jones' Locker](SJL/SJLApp/static/images/logo2.png)
---

The **Shady Jones' Locker** is a web app that facilitates the creation, modification, sharing and viewing of WebGL Shaders. Users can easily create beatiful shaders with dynamic uniform animations and provide input on the shaders of other users.

---

**Technologies Used:**
 - Django Backend (Python Package)
 - Various Javascript Libraries (ie. React, WebGL)
 - Bootstrap CSS/JS/Icon Library
 - Postgres Database

--- 

**Manuals:**
 - [Testing Manual](documentation/Testing Manual.pdf)

---

**Running on Local Machine:**
 1. Install [Python](https://www.python.org/downloads/) version 3.9 or above.
 2. Clone this repository.
 3. Create a [Python virtual environment](https://docs.python.org/3/tutorial/venv.html) in the root directory of the cloned repository. *(Optional, but recommended)*
    * `python3 -m venv env`
    * For Windows: `env\Scripts\activate.bat`
    * For Linux: `venv\bin\activate.bat`
 4. Intsall necessary packages by running pip install on the [requirement.txt](requirements.txt) file.
    * `pip install -r requirements.txt`
 5. Finally, you can run from the root directory via:
    * 'python3 SJL/manage.py runserver'

---

Visit [The Shady Jones' Locker](https://shadyjoneslocker.herokuapp.com/) to get started.
