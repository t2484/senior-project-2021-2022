from time import sleep
from django.urls import reverse
from django.contrib.auth.models import User
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support import ui
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from SJLApp.models import *

class CreateComment(LiveServerTestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
        }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()
        self.selenium = webdriver.Chrome()
        self.wait = WebDriverWait(self.selenium, 10)

    def test_create(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk':self.shader.id}))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@data-bs-target='#comments']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='comments']//textarea[@name='content']"))).send_keys("Comment!")
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='comments']//button[contains(text(), 'Submit')]"))).click()
        assert "Comment!" in self.selenium.find_element_by_xpath("//div[@id='comments']").get_attribute("innerHTML")

class ViewComment(LiveServerTestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
        }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader(user=self.user)
        self.shader.save()
        self.comment = Comment(content="Comment!", shader=self.shader, user=self.user)
        self.comment.save()
        self.selenium = webdriver.Chrome()
        self.wait = WebDriverWait(self.selenium, 10)

    def test_viewer(self):
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk':self.shader.id}))
        assert self.comment.content in self.selenium.find_element_by_xpath("//div[@id='comments']").get_attribute("innerHTML")
        assert "Delete" not in self.selenium.find_element_by_xpath("//div[@id='comments']").get_attribute("innerHTML")

    def test_owner(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk':self.shader.id}))
        assert self.comment.content in self.selenium.find_element_by_xpath("//div[@id='comments']").get_attribute("innerHTML")
        assert "Delete" in self.selenium.find_element_by_xpath("//div[@id='comments']").get_attribute("innerHTML")

class DeleteComment(LiveServerTestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
        }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader(user=self.user)
        self.shader.save()
        self.comment = Comment(content="Comment!", shader=self.shader, user=self.user)
        self.comment.save()
        self.selenium = webdriver.Chrome()
        self.wait = WebDriverWait(self.selenium, 10)

    def test_delete(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk':self.shader.id}))
        assert self.comment.content in self.selenium.find_element_by_xpath("//div[@id='comments']").get_attribute("innerHTML")
        assert "Delete" in self.selenium.find_element_by_xpath("//div[@id='comments']").get_attribute("innerHTML")
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@data-bs-target='#comments']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='comments']//button[contains(text(), 'Delete')]"))).click()
        assert self.comment.content not in self.selenium.find_element_by_xpath("//div[@id='comments']").get_attribute("innerHTML")
        assert "Delete" not in self.selenium.find_element_by_xpath("//div[@id='comments']").get_attribute("innerHTML")
