var c1;
var counter = 1;

class config{
    constructor(){
        this.menuOrder = [];
        this.controls = {};
    }

    addControl(control, name){
        this.controls[name] = control;
    }

    removeControl(name){
        var control = this.controls[name];
        delete this.controls[name];
    }
}

class range{
    constructor(max, min){
        this.type = "range";
        this.min = min;
        this.max = max;
    }
}

class select{
    constructor(options){
        this.type = "select";
        this.options = options;
    }
}

$(document).ready(function(){
    //Get the config.json file.
    var json;//= document.getElementById("config-json");
    load(json);
})

function load(json){
    /*
    The load function is called when the config.json file is loaded.
    Is it possible to call the load function more than once? Nope!
    No need to address this edge case.
    */
    c1 = new config();
    //For testing purposes, the local varaible json is hard coded.
    json = '{"menuOrder":["colorset","brightness","speed","exponent","supersamples"],"controls":{"brightness":{"type":"range","min":1,"max":8},"colorset":{"type":"select","options":["linear","squared periodic"]},"exponent":{"type":"range","min":0,"max":10},"speed":{"type":"range","min":0,"max":320},"supersamples":{"type":"select","options":{"1":"1x","4":"4x","16":"16x"}}}}';
    try{
        var _config = JSON.parse(json);
        var controls = _config.controls;
        var keys = Object.keys(controls);
        for(var i = 0; i < keys.length; i++){
            var name = keys[i];
            var type;
            eval('type = controls.' + name + '.type;')
            if(type == "range"){
                var max;
                eval('max = controls.' + name + '.max;');
                var min;
                eval('min = controls.' + name + '.min;');
                var r1 = new range(max, min);
                c1.addControl(r1, name);
            }
            else if(type == "select"){
                var options;
                eval('options = controls.' + name + '.options;');
                var s1 = new select(options);
                c1.addControl(s1, name);
            }
        }
    }
    catch(error){
        //If the config.json is null, it has a syntax error.
        //Every new config.json file is null. See the dilemma?
        console.log(error);
    }
    
    /*
    Using jQuery, store the config javascript object in the HTML document.
    Then, instantiate the config editor using the data from the config javascript object in the HTML document.
    */
    
    var keys = Object.keys(c1.controls);
    for(var i = 0; i < keys.length; i++){
        var id = "control-div-" + counter;
        $("#config-div").append('<div class="control-div" id="' + id + '"></div>');
        var key = keys[i];
        var control = c1.controls[key];
        $("#" + id).data("name", key);//https://api.jquery.com/data/#name
        var type = control["type"];
        $("#" + id).data("type", type);
        var min = control["min"];
        $("#" + id).data("min", min);
        var max = control["max"];
        $("#" + id).data("max", max);
        var options = control["options"];
        $("#" + id).data("options", options);

        var counter1 = 1;
        var id1 = "control-span-" + counter + "-";
        $("#" + id).append('<span class="control-span" id="' + id1 + counter1 + '">' + $("#" + id).data("name") + '</span>');
        counter1 += 1;
        $("#" + id).append('<span class="control-span" id="' + id1 + counter1 + '">' + $("#" + id).data("type") + '</span>');
        counter1 += 1;
        $("#" + id).append('<span class="control-input" id="' + id1 + counter1 + '">' + $("#" + id).data("min") + '</span>');
        counter1 += 1;
        $("#" + id).append('<span class="control-span" id="' + id1 + counter1 + '">' + $("#" + id).data("max") + '</span>');
        counter1 += 1;
        $("#" + id).append('<span class="control-span" id="' + id1 + counter1 + '">' + $("#" + id).data("options") + '</span>');
        counter1 += 1;

        if($("#" + id).data("type") == "range"){
            $("#" + id1 + "5").hide();
        }
        else if($("#" + id).data("type") == "select"){
            $('#' + id1 + "3").hide();
            $('#' + id1 + "4").hide();
        }

        /*
        console.log($("#" + id).data("name"));
        console.log($("#" + id).data("type"));
        console.log($("#" + id).data("min"));
        console.log($("#" + id).data("max"));
        console.log($("#" + id).data("options"));
        */

        counter += 1;
    }

    //$("#" + id).append('<button onclick="">New Range</button>');
}

function save(){
    /*
    Verify that the config file is good to go!
    Upload the config file and update the database.
    */
    var json = JSON.stringify(c1);
    $("#config-json").val(json);

}