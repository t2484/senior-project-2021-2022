var editor;

//https://learn.jquery.com/using-jquery-core/document-ready/
//A webpage cannot be manipulated safely until the document is "ready."
//jQuery detects this state of readiness for us.
//Code included inside $(document).ready() will only run once the page Document Object Model (DOM) is ready for the JavaScript program to execute.
$(document).ready(function(){
    var code = $("#editor-id")[0];
    //var shaderCodeElement = document.getElementById("shader-code");
    //var shaderCode = shaderCodeElement.getValue();
    var shaderCode = $("#shader-code").val();
    editor = CodeMirror(code, {
        lineNumbers : true,
        value : shaderCode,
        autoRefresh : true,
        fixedGutter:true,
        lint:true, 
        coverGutterNextToScrollbar:false,
        gutters: ['CodeMirror-lint-markers'],
    });
    refreshMyEditor();
    //Set the size of the editor to the size of the element.
    //var element = document.getElementById("my-id")
    //editor.setSize(element.offsetWidth, element.offsetHeight)
})

$("#get").click(function(){
    var myField = document.getElementById("shader-code");
    myField.value = editor.getValue();
});

function refreshMyEditor(){
    editor.refresh();   
    //alert("REFRESH");
    setTimeout(function(){
        editor.refresh();
    }, 1000);
}