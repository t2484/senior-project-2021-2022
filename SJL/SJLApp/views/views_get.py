from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.core.paginator import Paginator
from django.core.exceptions import FieldError
from django.contrib import messages
from django.contrib.auth.models import User
from django.views.decorators.http import require_http_methods
from SJLApp.forms import *
from SJLApp.models import Shader

SHADER_INDEX_PAGE_SIZE = 9

def import_global_context():
    context = {}
    context['login_form'] = LoginForm()
    return context

def filter_by_query_parameters(queryset, querydict):
    q_object = None
    for key in querydict:
        if not q_object:
            q_object = Q(**{key: querydict.get(key)})
        q_object &= Q(**{key: querydict.get(key)})
    if q_object:
        return queryset.filter(q_object)
    else:
        return queryset

def error404(request, *args, **argv):
    return render(request, 'SJLApp/404.html', status=404)

@require_http_methods(["GET"])
def about_us(request):
    context = import_global_context()
    return render(request, 'SJLApp/aboutUs.html', context=context)

#Page for logged in user
@require_http_methods(["GET"])
def user(request):
    context = import_global_context()
    user = request.user
    if user.is_authenticated:
        for key in request.GET:
            context[key] = request.GET.get(key)
        context['this_user'] = user
        context['querystring'] = request.GET.urlencode()
        context['change_pass_form'] = ChangePasswordForm(user)
        context['change_user_info_form'] = ChangeUserInfoForm(instance=user)
        context['about_form'] = AboutForm(instance=user.profile)
        return render(request, 'SJLApp/user.html', context=context)
    else:
        return redirect(reverse_lazy("SJLApp:login_page"))

#Gets user by pk
@require_http_methods(["GET"])
def any_user(request, pk):
    context = import_global_context()
    if User.objects.filter(pk=pk).exists():
        user = User.objects.get(pk=pk)
        for key in request.GET:
            context[key] = request.GET.get(key)
        context['querystring'] = request.GET.urlencode()
        context['this_user'] = user
        if request.user == user:
            context['change_pass_form'] = ChangePasswordForm(user)
            context['change_user_info_form'] = ChangeUserInfoForm(instance=user)
            context['about_form'] = AboutForm(instance=user.profile)
        return render(request, 'SJLApp/user.html', context=context)
    elif request.user.is_authenticated:
        messages.error(request, "User not found")
        return redirect(reverse_lazy("SJLApp:user"))
    else:
        return error404(request)

# List of shaders
@require_http_methods(["GET"])
def index(request):
    context = import_global_context()
    for key in request.GET:
        context[key] = request.GET.get(key)
    context['querystring'] = request.GET.urlencode()
    return render(request, 'SJLApp/index.html', context=context)

# Specific view of shader
@require_http_methods(["GET"])
def shader_detail(request, pk):
    context = import_global_context()
    if Shader.objects.filter(pk=pk).exists():
        shader = Shader.objects.get(pk=pk)
        user = request.user
        if shader.is_public or user == shader.user:
            context['shader'] = shader
            context['shader_form'] = ShaderForm(instance=shader)
            context['comment_form'] = CommentForm()
            if(user == shader.user):
                return render(request, 'SJLApp/detailOwner.html', context=context)
            else:
                return render(request, 'SJLApp/detailViewer.html', context=context)
        else:
            messages.error(request, "Access Denied")
            return redirect(reverse_lazy("SJLApp:index"))
    else:
        messages.error(request, "Shader not found.")
        return redirect(reverse_lazy("SJLApp:index"))

@require_http_methods(["GET"])
def create_user_page(request):
    if request.user.is_authenticated:
        return redirect(reverse_lazy('SJLApp:user'))
    else:
        context = import_global_context()
        context['create_user_form'] = UserCreateForm()
        return render(request, 'SJLApp/createUser.html', context=context)

@require_http_methods(["GET"])
def user_login_page(request):
    if request.user.is_authenticated:
        messages.error(request, "Already logged in")
        return redirect(reverse_lazy("SJLApp:user"))
    else:
        context = import_global_context()
        context['login_form'] = LoginForm()
        return render(request, 'SJLApp/login.html', context=context)

@require_http_methods(["GET"])
def api_shaders(request):
    querydict = request.GET.copy()
    if 'page' in querydict:
        pg = querydict.pop('page')[0]
    else:
        pg = 1
    shaders = Shader.objects.filter(is_public=True).order_by('pk')
    try:
        shaders = filter_by_query_parameters(shaders, querydict)
    except FieldError:
        messages.error(request, "Invalid Request")
        return redirect(reverse_lazy("SJLApp:index"))
    paginator = Paginator(shaders, SHADER_INDEX_PAGE_SIZE)
    data = list()
    for shader in paginator.get_page(pg):
        dict = {
            'id': shader.pk,
            'name': shader.title,
            'code': shader.code,
            'config': shader.config,
        }
        data.append(dict)
    response = JsonResponse(data, safe=False)
    return response

@require_http_methods(["GET"])
def api_shader(request, pk):
    if Shader.objects.filter(pk=pk).exists():
        shader = Shader.objects.get(pk=pk)
        if shader.is_public or request.user == shader.user:
            data = []
            dict = {
                'id': shader.pk,
                'name': shader.title,
                'code': shader.code,
                'config': shader.config,
            }
            data.append(dict)
            response = JsonResponse(data, safe=False)
            return response
        else:
            messages.error(request, "Access denied")
            return redirect(reverse_lazy("SJLApp:login_page"))
    else:
        return error404(request)

@require_http_methods(["GET"])
def api_user_shaders(request, pk):
    if User.objects.filter(pk=pk).exists():
        querydict = request.GET.copy()
        if 'page' in querydict:
            pg = querydict.pop('page')[0]
        else:
            pg = 1
        user = User.objects.get(pk=pk)
        if request.user == user:
            shaders = Shader.objects.filter(user=user).order_by('pk')
        else:
            shaders = Shader.objects.filter(user=user, is_public=True).order_by('pk')
        try:
            shaders = filter_by_query_parameters(shaders, querydict)
        except FieldError:
            messages.error(request, "Invalid Request")
            return redirect(reverse_lazy("SJLApp:index"))
        paginator = Paginator(shaders, SHADER_INDEX_PAGE_SIZE)
        data = []
        for shader in paginator.get_page(pg):
            dict = {
                'id': shader.pk,
                'name': shader.title,
                'code': shader.code,
                'config': shader.config,
            }
            data.append(dict)
        response = JsonResponse(data, safe=False)
        return response
    else:
        return error404(request)

