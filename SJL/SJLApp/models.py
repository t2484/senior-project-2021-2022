from pickle import FALSE
from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver

#Extending the base Django user model via a one to one relation
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    about = models.TextField(max_length=500, blank=True)

    #Extended fields/methods go here

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

def defaultConfig():
    return {
            "menu": {
                "controls": {
                "red": {
                    "max": 1,
                    "min": 0,
                    "type": "range"
                },
                "green": {
                    "max": 1,
                    "min": 0,
                    "type": "range"
                },
                "blue": {
                    "max": 1,
                    "min": 0,
                    "type": "range"
                }
                },
                "menuOrder": [
                "red",
                "green",
                "blue"
                ]
            },
            "settings": {
                "viewport": {
                "range": {
                    "x": 4,
                    "y": 4
                },
                "center": {
                    "x": 0,
                    "y": 0
                },
                "rotation": 0
                },
                "rangeSettings": {
                "red": {
                    "max": 1,
                    "min": 0,
                    "phase": 0,
                    "value": 0,
                    "animated": False,
                    "amplitude": 1,
                    "frequency": 0.2
                },
                "blue": {
                    "max": 1,
                    "min": 0,
                    "phase": 2,
                    "value": 0,
                    "animated": False,
                    "amplitude": 1,
                    "frequency": 0.2
                },
                "green": {
                    "max": 1,
                    "min": 0,
                    "phase": 4,
                    "value": 0,
                    "animated": False,
                    "amplitude": 1,
                    "frequency": 0.2
                }
                },
                "selectSettings": {}
            }
        }

defaultCode = "precision mediump float;\n\nuniform float GREEN;\nuniform float RED;\nuniform float BLUE;\n\nvoid main(){\n\tvec3 color = vec3(RED, BLUE, GREEN);\n\tgl_FragColor = vec4(color,1.0);\n}"

class Shader(models.Model):
    title = models.CharField(default='Default Title', max_length=64)
    description = models.TextField(default='Default Description', max_length=256)
    date = models.DateTimeField(auto_now_add=True)
    is_public = models.BooleanField(default=True)
    views = models.IntegerField(default=0)
    code = models.TextField(default=defaultCode, max_length=10000)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    config = models.JSONField(default=defaultConfig)

    def __str__(self):
        return self.title

class Comment(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    content = models.CharField(blank=True, max_length=256)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    shader = models.ForeignKey(Shader, on_delete=models.CASCADE)


