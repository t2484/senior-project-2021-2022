from time import sleep
from django.urls import reverse
from django.contrib.auth.models import User
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.common.by import By
from SJLApp.models import *

class CreateShader(LiveServerTestCase):
  def setUp(self):
    self.credentials = {
      'username': 'username',
      'password': 'password'
    }
    self.user = User.objects.create_user(**self.credentials)
    self.selenium = webdriver.Chrome()
    self.wait = WebDriverWait(self.selenium, 10)

  def test_unauthenticated(self):
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@data-bs-target='#offcanvasNavbar']"))).click()
    parent = self.selenium.find_elements_by_xpath("//div[@id='offcanvasNavbar']")[0]
    WebDriverWait(parent, 10).until(EC.element_to_be_clickable((By.XPATH, ".//button[contains(text(), 'Create Shader')]"))).click()
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:login_page'))

  def test_authenticated_from_nav(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@data-bs-target='#offcanvasNavbar']"))).click()
    parent = self.selenium.find_elements_by_xpath("//div[@id='offcanvasNavbar']")[0]
    WebDriverWait(parent, 10).until(EC.element_to_be_clickable((By.XPATH, ".//button[contains(text(), 'Create Shader')]"))).click()
    assert self.selenium.current_url.__contains__(self.live_server_url+'/shader/')
    assert 'Default Title' in self.selenium.page_source

  def test_authenticated_from_dropdown(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@data-bs-toggle='dropdown']"))).click()
    parent = self.selenium.find_elements_by_xpath("//div[contains(@class, 'dropdown-menu')]")[0]
    WebDriverWait(parent, 10).until(EC.element_to_be_clickable((By.XPATH, ".//button[contains(text(), 'Create Shader')]"))).click()
    assert self.selenium.current_url.__contains__(self.live_server_url+'/shader/')

  def test_authenticated_from_profile(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@data-bs-toggle='dropdown']"))).click()
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[contains(text(), 'My Profile')]"))).click()
    parent = self.selenium.find_elements_by_xpath("//div[@id='nav-tab']")[0]
    WebDriverWait(parent, 10).until(EC.element_to_be_clickable((By.XPATH, ".//button[contains(text(), 'Create Shader')]"))).click()
    assert self.selenium.current_url.__contains__(self.live_server_url+'/shader/')
    assert 'Default Title' in self.selenium.page_source

class ViewShader(LiveServerTestCase):
  def setUp(self):
    self.credentials = {
      'username': 'username',
      'password': 'password'
    }
    self.user = User.objects.create_user(**self.credentials)
    self.shaders = {}
    for i in range(10):
      self.shaders[i] = Shader(title=f"Title{i}", user=self.user)
      self.shaders[i].save()
    self.selenium = webdriver.Chrome()
    self.wait = WebDriverWait(self.selenium, 10)

  def test_index_search(self):
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    assert 'Title1' in self.selenium.page_source
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@id='search-input']"))).send_keys("9")
    self.selenium.execute_script('arguments[0].click()',self.selenium.find_elements_by_xpath("//button[@id='search-btn']")[0])
    assert 'Title9' in self.selenium.page_source
    assert not 'Title1' in self.selenium.page_source

  def test_index_pages(self):
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    assert 'Title1' in self.selenium.page_source
    self.selenium.execute_script('arguments[0].click()',self.selenium.find_elements_by_xpath("//button[contains(@class, 'next-btn')]")[0])
    assert 'Title9' in self.selenium.page_source
    self.selenium.execute_script('arguments[0].click()',self.selenium.find_elements_by_xpath("//button[contains(@class, 'prev-btn')]")[0])
    assert 'Title1' in self.selenium.page_source

  def test_unauthenticated(self):
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shaders[0].id}))
    assert self.shaders[0].title in self.selenium.page_source
    assert 'Config' not in self.selenium.page_source

  def test_authenticated(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shaders[0].id}))
    assert self.shaders[0].title in self.selenium.page_source
    assert 'Config' in self.selenium.page_source

class UpdateShader(LiveServerTestCase):
  def setUp(self):
    self.credentials = {
      'username': 'username',
      'password': 'password'
    }
    self.user = User.objects.create_user(**self.credentials)
    self.shader = Shader()
    self.shader.user = self.user
    self.shader.save()
    self.selenium = webdriver.Chrome()
    self.wait = WebDriverWait(self.selenium, 10)

  def test_title(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
    assert 'Default Title' in self.selenium.page_source
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='shader-title-edit']"))).click()
    input = self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@id='shader-title']")))
    input.clear()
    input.send_keys("New Default Title")
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='shader-title-save']"))).click()
    assert 'New Default Title' in self.selenium.page_source
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
    assert 'New Default Title' in self.selenium.page_source

  def test_description(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
    assert 'Default Description' in self.selenium.page_source
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='shader-description-edit']"))).click()
    input = self.wait.until(EC.visibility_of_element_located((By.XPATH, "//textarea[@id='shader-description']")))
    input.clear()
    input.send_keys("New Default Description")
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='shader-description-save']"))).click()
    assert 'New Default Description' in self.selenium.page_source
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
    assert 'New Default Description' in self.selenium.page_source

  def test_public(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
    assert 'Default Description' in self.selenium.page_source
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@id='shader-ispublic']"))).click()
    sleep(1)
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    assert 'Default Title' not in self.selenium.page_source

  def test_code(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@data-bs-target='#glsl']"))).click()
    codemirror = self.wait.until(EC.presence_of_element_located((By.XPATH, "//*[contains(@class, 'CodeMirror')]")))
    self.selenium.execute_script("arguments[0].CodeMirror.setValue(\"" + "void main(){gl_FragColor = vec4(0.1, 0.1, 0.1, 1.0);}" + "\");", codemirror)
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='shader-code-save']"))).click()
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
    assert 'void main(){gl_FragColor = vec4(0.1, 0.1, 0.1, 1.0);}' in self.selenium.page_source

  def test_config(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@data-bs-target='#config']"))).click()
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='control-button-1-10']"))).click()
    assert not self.selenium.find_elements_by_xpath("//div[@id='config']//*[contains(text(), 'red')]")
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Add Control')]"))).click()
    self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@id='control-input-4-1']"))).send_keys("NewRange")
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Add Control')]"))).click()
    self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@id='control-input-5-1']"))).send_keys("NewOption")
    Select(self.wait.until(EC.visibility_of_element_located((By.XPATH, "//select[@id='control-select-5-2']")))).select_by_visible_text('select')
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='control-button-5-5']"))).click()
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Save Config')]"))).click()
    sleep(1)
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk': self.shader.id}))
    assert 'NewRange' in self.selenium.page_source
    assert 'NewOption' in self.selenium.page_source

class DeleteShader(LiveServerTestCase):
  def setUp(self):
    self.credentials = {
      'username': 'username',
      'password': 'password'
    }
    self.user = User.objects.create_user(**self.credentials)
    self.shader = Shader()
    self.shader.user = self.user
    self.shader.save()
    self.selenium = webdriver.Chrome()
    self.wait = WebDriverWait(self.selenium, 10)

  def test_delete(self):
    self.client.login(**self.credentials)
    cookie = self.client.cookies['sessionid']
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
    self.selenium.refresh()
    self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
    self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
    self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Delete')]"))).click()
    sleep(1)
    assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:user'))
    assert "Default Title" not in self.selenium.page_source
