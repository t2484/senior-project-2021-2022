var c1;
var counter = 1;

class config{
    constructor(){
        this.menuOrder = [];
        this.controls = {};
    }
}

class range{
    constructor(min, max){
        this.type = "range";
        this.min = min;
        this.max = max;
    }
}

class select{
    constructor(options){
        this.type = "select";
        this.options = options;
    }
}

class settings{
    constructor(rangeSettings, selectSettings){
        var range = {x : 1, y : 1};
        var center = {x : 0, y : 0};
        var rotation = 0;
        this.viewport = {range, center, rotation};
        this.rangeSettings = rangeSettings;
        this.selectSettings = selectSettings;
    }
}

class rangeSetting{
    constructor(min, max){
        this.min = min;
        this.max = max;
        this.animated = false;
        this.amplitude = 1;
        this.phase = 0;
        this.frequency = 0.2;
        this.value = min;
    }
}

class selectSetting{

}

class menu{
    constructor(config, settings){
        this.menu = config;
        this.settings = settings;
    }
}

function constructSettings(){
    var controls = c1.controls;
    var keys = Object.keys(controls);
    var ranges = {};
    var selects = {};
    for(var i = 0; i < keys.length; i++){
        var control = controls[keys[i]];
        var type = control.type;
        if(type == "range"){
            var r1 = new rangeSetting(control.min, control.max);
            ranges[keys[i]] = r1;
        }
        else if(type == "select"){
            //var s1 = new selectSetting();
            //selects[keys[i]] = s1;
        }
    }

    var s1 = new settings(ranges, selects);
    return s1;
}

$(document).ready(function(){
    var json = document.getElementById("shader-config");
    json = $(json).val();
    //json = '{"menu": {"controls": {"colorset": {"type": "select", "options": ["linear", "squared periodic"]}, "exponent": {"max": 10, "min": 0, "type": "range"}, "brightness": {"max": 8, "min": 1, "type": "range"}, "supersamples": {"type": "select", "options": {"1": "1x", "4": "4x", "16": "16x"}}}, "menuOrder": ["colorset", "brightness", "exponent", "supersamples"]}, "settings": {"viewport": {"range": {"x": 4, "y": 4}, "center": {"x": 0, "y": 0}, "rotation": 0}, "rangeSettings": {"speed": {"max": 320, "min": 0, "phase": 0, "value": 16, "animated": "false", "amplitude": 1, "frequency": 0.2}, "exponent": {"max": 10, "min": 0, "phase": 0, "value": 2, "animated": "false", "amplitude": 1, "frequency": 0.2}, "brightness": {"max": 8, "min": 1, "phase": 0, "value": 4, "animated": "false", "amplitude": 1, "frequency": 0.2}}, "selectSettings": {"colorset": 0, "supersamples": 16}}}';
    //json = '{"menu": {"controls": {"colorset": {"type": "select", "options": ["linear", "squared periodic"]}, "exponent": {"max": 10, "min": 0, "type": "range"}, "brightness": {"max": 8, "min": 1, "type": "range"}, "supersamples": {"type": "select", "options": {"1": "1x", "4": "4x", "16": "16x"}}}, "menuOrder": ["colorset", "brightness", "exponent", "supersamples"]}, "settings": {"viewport": {"range": {"x": 4, "y": 4}, "center": {"x": 0, "y": 0}, "rotation": 0}, "rangeSettings": {"speed": {"max": 320, "min": 0, "phase": 0, "value": 16, "animated": "false", "amplitude": 1, "frequency": 0.2}, "exponent": {"max": 10, "min": 0, "phase": 0, "value": 2, "animated": "false", "amplitude": 1, "frequency": 0.2}, "brightness": {"max": 8, "min": 1, "phase": 0, "value": 4, "animated": "false", "amplitude": 1, "frequency": 0.2}}, "selectSettings": {"colorset": 0, "supersamples": 16}}}';
    //json = '{"menu":{"menuOrder":["colorset","exponent","brightness","supersamples"],"controls":{"colorset":{"type":"select","options":""},"exponent":{"type":"range","min":"0","max":"10"},"brightness":{"type":"range","min":"1","max":"8"},"supersamples":{"type":"select","options":""}},"settings":{"viewport":{"range":{"x":1,"y":1},"center":{"x":0,"y":0},"rotation":0},"rangeSettings":{"exponent":{"min":"0","max":"10","animated":false,"amplitude":1,"phase":0,"frequency":0.2,"value":"0"},"brightness":{"min":"1","max":"8","animated":false,"amplitude":1,"phase":0,"frequency":0.2,"value":"1"}},"selectSettings":{}}}}';
    console.log(json);
    load(json);
})

function load(json){
    c1 = new config();
    try{
        var menu = JSON.parse(json);
        _config = menu.menu;
        var controls = _config.controls;
        var keys = Object.keys(controls);
        for(var i = 0; i < keys.length; i++){
            var type = controls[keys[i]].type;
            if(type == "range"){
                var min = controls[keys[i]].min;
                var max = controls[keys[i]].max;
                c1.controls[keys[i]] = new range(min, max);
            }
            else if(type == "select"){
                var options = controls[keys[i]].options;
                c1.controls[keys[i]] = new select(options);
            }
            c1.menuOrder.push(keys[i]);
        }
    }
    catch(error){
        console.error(error);
    }
    
    for(var i = 0; i < keys.length; i++){
        var id = "control-div-" + counter;
        $("#config-div").append('<div class="control-div" id="' + id + '"></div>');
        $("#" + id).data("counter", counter);
        var key = keys[i];
        var control = c1.controls[key];
        $("#" + id).data("name", key);//https://api.jquery.com/data/#name
        var type = control["type"];
        $("#" + id).data("type", type);
        var min = control["min"];
        $("#" + id).data("min", min);
        var max = control["max"];
        $("#" + id).data("max", max);
        var options = control["options"];
        $("#" + id).data("options", options);

        var counter1 = 1;
        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">name</span>');
        $("#" + id).append('<input type="text" class="control-input" id="control-input-' + counter + "-" + counter1 + '">' + '</input>');
        $("#" + "control-input-" + counter + "-" + counter1).val($("#" + id).data("name"));
        counter1 += 1;

        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">type</span>');
        $("#" + id).append('<select class="control-select" id="control-select-' + counter + '-' + counter1 + '" onchange="setType(this)"><option class="control-option" value="range">range</option><option class="control-option" value="select">select</option></select>');
        $("#" + "control-select-" + counter + "-" + counter1).val($("#" + id).data("type"));
        counter1 += 1;

        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">min</span>');
        $("#" + id).append('<input type="number" class="control-input" id="control-input-' + counter + "-" + counter1 + '">' + '</input>');
        $("#" + "control-input-" + counter + "-" + counter1).val($("#" + id).data("min"));
        counter1 += 1;

        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">max</span>');
        $("#" + id).append('<input type="number" class="control-input" id="control-input-' + counter + "-" + counter1 + '">' + '</input>');
        $("#" + "control-input-" + counter + "-" + counter1).val($("#" + id).data("max"));
        counter1 += 1;
        
        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">options</span>');
        $("#" + "control-span-" + counter + "-" + counter1).data("counter", 1);
        $("#" + id).append('<button class="control-button" id="control-button-' + counter + '-' + counter1 + '" onclick="addOption(this, 0, \'zero\')">Add Option</button>');
        if(options != null){
            var keys1 = Object.keys(options);
            for(var j = 0; j < keys1.length; j++){
                addOption($("#" + id), keys1[j], options[keys1[j]]);
            }
        }
        counter1 += 1;

        $("#" + id).append('<button class="control-button" id="control-button-' + counter + '-' + counter1 + '" onclick="removeControl(this)">Remove Control</button>');
        counter1 += 1;
        
        if($("#" + id).data("type") == "range"){
            $("#" + "control-span-" + counter + "-5").hide();
            $("#" + "control-button-" + counter + "-5").hide();
        }
        else if($("#" + id).data("type") == "select"){
            $("#" + "control-span-" + counter + "-3").hide();
            $("#" + "control-input-" + counter + "-3").hide();
            $("#" + "control-span-" + counter + "-4").hide();
            $("#" + "control-input-" + counter + "-4").hide();
        }
        counter += 1;
    }
}

function addControl(){
    var id = "control-div-" + counter;
        $("#config-div").append('<div class="control-div" id="' + id + '"></div>');
        $("#" + id).data("counter", counter);
        $("#" + id).data("name", "");//https://api.jquery.com/data/#name
        $("#" + id).data("type", "range");
        $("#" + id).data("min", 0);
        $("#" + id).data("max", 0);
        $("#" + id).data("options", "");

        var counter1 = 1;
        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">name</span>');
        $("#" + id).append('<input type="text" class="control-input" id="control-input-' + counter + "-" + counter1 + '">' + '</input>');
        $("#" + "control-input-" + counter + "-" + counter1).val($("#" + id).data("name"));
        counter1 += 1;

        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">type</span>');
        $("#" + id).append('<select class="control-select" id="control-select-' + counter + '-' + counter1 + '" onchange="setType(this)"><option class="control-option" value="range">range</option><option class="control-option" value="select">select</option></select>');
        $("#" + "control-select-" + counter + "-" + counter1).val($("#" + id).data("type"));
        counter1 += 1;

        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">min</span>');
        $("#" + id).append('<input type="number" class="control-input" id="control-input-' + counter + "-" + counter1 + '">' + '</input>');
        $("#" + "control-input-" + counter + "-" + counter1).val($("#" + id).data("min"));
        counter1 += 1;

        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">max</span>');
        $("#" + id).append('<input type="number" class="control-input" id="control-input-' + counter + "-" + counter1 + '">' + '</input>');
        $("#" + "control-input-" + counter + "-" + counter1).val($("#" + id).data("max"));
        counter1 += 1;
        
        $("#" + id).append('<span class="control-span" id="control-span-' + counter + '-' + counter1 + '">options </span>');
        $("#" + "control-span-" + counter + "-" + counter1).data("counter", 1);
        $("#" + id).append('<button class="control-button" id="control-button-' + counter + '-' + counter1 + '" onclick="addOption(this, 0, \'zero\')">Add Option</button>');
        counter1 += 1;

        $("#" + id).append('<button class="control-button" id="control-button-' + counter + '-' + counter1 + '" onclick="removeControl(this)">Remove Control</button>');
        counter1 += 1;
        
        if($("#" + id).data("type") == "range"){
            $("#" + "control-span-" + counter + "-5").hide();
            $("#" + "control-button-" + counter + "-5").hide();
        }
        else if($("#" + id).data("type") == "select"){
            $("#" + "control-span-" + counter + "-3").hide();
            $("#" + "control-input-" + counter + "-3").hide();
            $("#" + "control-span-" + counter + "-4").hide();
            $("#" + "control-input-" + counter + "-4").hide();
        }
        counter += 1;
}

function removeControl(button){
    var control = button.parentNode;
    $(control).remove();
}

function addOption(element, key, value){
    console.log(element);
    console.log("Add Option");
    console.log(key);
    console.log(value);
    if($(element).is("button")){
        element = element.parentNode;
    }
    console.log(element);
    var options = $('#control-span-' + $(element).data("counter") + '-5');
    $(options).append('<span class="contorl-option-span" id="control-option-span-' + $(element).data("counter") + '-' + $(options).data("counter") + '"></span>');
    var option = $("#" + "control-option-span-" + $(element).data("counter") + "-" + $(options).data("counter"));
    $(options).data("counter", $(options).data("counter") + 1);
    $(option).append('<span class="control-option-span">value</span>');
    $(option).append('<input class="control-option-key" type="number" value="' + key + '"></input>');
    $(option).append('<span class="control-option-span">name</span>');
    $(option).append('<input class="control-option-value" type="text" value="' + value + '"></input>');
    $(option).append('<button class="control-option-button" onclick="removeOption(this)">Remove Option</button>');
}

function removeOption(button){
    var option = button.parentNode;
    $(option).remove();
}

function update(){
    var c2 = new config();
    var controls = $("#config-div").find(".control-div");
    for(var i = 0; i < controls.length; i++){
        var control = controls[i];
        var _counter = $(control).data("counter");
        var name = $("#control-input-" + _counter + "-1").val();
        $(control).data("name", name);
        var type = $("#control-select-" + _counter + "-2").val();
        $(control).data("type", type);
        var min, max, options;
        if(type == "range"){
            min = $("#control-input-" + _counter + "-3").val();
            $(control).data("min", min);
            max = $("#control-input-" + _counter + "-4").val();
            $(control).data("max", max);
            c2.controls[name] = new range(min, max);
        }
        else if(type == "select"){

            options = {};
            var optionsElement = $("#control-span-" + _counter + "-5").children();
            for(var j = 0; j < optionsElement.length; j++){
                var option = optionsElement[j];
                var key = $(option).find(".control-option-key");
                var value = $(option).find(".control-option-value");
                options[$(key).val()] = $(value).val();
            }
            $(control).data("options", options);
            c2.controls[name] = new select(options);
        }
        c2.menuOrder.push(name);
    }
    c1 = c2;
}

function verify(){
    var valid = true;
    var controls = $("#config-div").find(".control-div");
    for(var i = 0; i < controls.length; i++){
        var control = controls[i];
        var _counter = $(control).data("counter");
        var name = $("#control-input-" + _counter + "-1").val();
        if(!(/^[a-zA-Z]+$/.test(name))){
            console.log(name + " is invalid.");
            alert(name + " is invalid.");
            valid = false;
        }
        var type = $("#control-select-" + _counter + "-2").val();
        if(!(type == "range" || type == "select")){
            console.log(type +  "is invalid.");
            alert(type + " is invalid.");
            valid = false;
        }
        if(type == "range"){
            var min = $("#control-input-" + _counter + "-3").val();
            if(!(/^-?[0-9]\d*(\.\d+)?$/.test(min))){
                console.log(min + " is invalid.");
                alert(min + " is invalid.");
                valid = false;
            }
            var max = $("#control-input-" + _counter + "-4").val();
            if(!(/^-?[0-9]\d*(\.\d+)?$/.test(max)) || max < min){
                console.log(max + " is invalid.");
                alert(max + " is invalid.");
                valid = false;
            }
        }
        else if(type == "select"){
            var options = $("#control-span-" + _counter + "-5").children();
            for(var j = 0; j < options.length; j++){
                var option = options[j];
                var key = $(option).find(".control-option-key");
                //test key
                if(!(/^-?[0-9]\d*(\.\d+)?$/.test($(key).val()))){
                    alert($(key).val() + " is invalid.");
                }

                var value = $(option).find(".control-option-value");
                if(!(/^[a-zA-Z0-9 ]+$/.test($(value).val()))){
                    alert($(value).val() + " is invalid.");
                }
            }
        }
    }
    return valid;
}

//
//
//

function save(){
    var valid = verify();
    if(!valid){
        console.log("Config is invalid.")
        alert("Config is invalid.");
        return;
    }
    update();
    var settings = constructSettings();
    var _menu = new menu(c1, settings);
    var json = JSON.stringify(_menu);
    console.log(json);
    $("#shader-config").val(json);
}

//
//
//

function setType(select){
    var type = select.value;
    if(!(type == "range" || type =="select")){
        console.error("");
    }
    var control = select.parentNode;
    $(control).data("type", type);
    var counter1 = $(control).data("counter");
    if(type == "range"){
        $("#" + "control-span-" + counter1 + "-3").show();
        $("#" + "control-input-" + counter1 + "-3").show();
        $("#" + "control-span-" + counter1 + "-4").show();
        $("#" + "control-input-" + counter1 + "-4").show();
        $("#" + "control-span-" + counter1 + "-5").hide();
        $("#" + "control-button-" + counter1 + "-5").hide();
    }
    else if(type == "select"){
        $("#" + "control-span-" + counter1 + "-3").hide();
        $("#" + "control-input-" + counter1 + "-3").hide();
        $("#" + "control-span-" + counter1 + "-4").hide();
        $("#" + "control-input-" + counter1 + "-4").hide();
        $("#" + "control-span-" + counter1 + "-5").show();
        $("#" + "control-button-" + counter1 + "-5").show();
    }
    else{
        console.error(type + " is invalid.");
    }
}
