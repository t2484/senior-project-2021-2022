from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from SJLApp.forms import *
from SJLApp.models import Shader, Comment


@require_http_methods(["POST"])
@login_required(login_url=reverse_lazy('SJLApp:login_page'))
def create_shader(request):
    shader = Shader()
    shader.user = request.user
    shader.save()
    return redirect(reverse('SJLApp:shader_detail_page', kwargs={'pk': shader.pk}))

@require_http_methods(["POST"])
@login_required(login_url=reverse_lazy('SJLApp:login_page'))
def update_shader(request, pk):
    if Shader.objects.filter(pk=pk).exists():
        shader = Shader.objects.get(pk=pk)
        if request.user == shader.user:
            shader_form = ShaderForm(request.POST, instance=shader)
            if shader_form.is_valid():
                shader_form.save()
                return redirect(reverse('SJLApp:shader_detail_page', kwargs={'pk': shader.pk}))
            else:
                for errors in shader_form.errors.values():
                    for error in errors:
                        messages.error(request, error)
                return redirect(request.META.get('HTTP_REFERER', 'SJLApp:home'))

@require_http_methods(["POST"])
@login_required(login_url=reverse_lazy('SJLApp:login_page'))
def delete_shader(request, pk):
    if Shader.objects.filter(pk=pk).exists():
        shader = Shader.objects.get(pk=pk)
        if request.user == shader.user:
            shader.delete()
            return redirect(reverse_lazy('SJLApp:user'))

@require_http_methods(["POST"])
@login_required(login_url=reverse_lazy('SJLApp:login_page'))
def create_comment(request, pk):
    if Shader.objects.filter(pk=pk).exists():
        shader = Shader.objects.get(pk=pk)
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            data = comment_form.cleaned_data
            comment = Comment(**data)
            comment.shader = shader
            comment.user = request.user
            comment.save()
        else:
            for errors in comment_form.errors.values():
                for error in errors:
                    messages.error(request, error)
        return redirect(reverse_lazy('SJLApp:shader_detail_page', kwargs={'pk': shader.pk}))

@require_http_methods(["POST"])
@login_required(login_url=reverse_lazy('SJLApp:login_page'))
def delete_comment(request, pk):
    if Comment.objects.filter(pk=pk).exists():
        comment = Comment.objects.get(pk=pk)
        if request.user == comment.user:
            comment.delete()
            return redirect(request.META.get('HTTP_REFERER', 'SJLApp:home'))            

@require_http_methods(["POST"])
def create_user(request):
    user_create_form = UserCreateForm(request.POST)
    if user_create_form.is_valid():
        email = user_create_form.cleaned_data['email']
        username = user_create_form.cleaned_data['username']
        password = user_create_form.cleaned_data['password']
        User.objects.create_user(username = username, password = password, email = email)
        messages.success(request, 'User successfully created, please log in.')
        return redirect(reverse_lazy('SJLApp:login_page'))
    else:
        for errors in user_create_form.errors.values():
            for error in errors:
                messages.error(request, error)
        return redirect(request.META.get('HTTP_REFERER', 'SJLApp:home'))

@require_http_methods(["POST"])
@login_required(login_url=reverse_lazy('SJLApp:login_page'))
def change_user_info(request):
    user = request.user
    change_user_info_form = ChangeUserInfoForm(request.POST, instance=user)
    if change_user_info_form.is_valid():
        first_name = change_user_info_form.cleaned_data.get('first_name')
        last_name = change_user_info_form.cleaned_data.get('last_name')
        email = change_user_info_form.cleaned_data.get('email')
        if first_name:
            user.first_name = first_name
        if last_name:
            user.last_name = last_name
        if email:
            user.email = email
        user.save()
    else:
        for errors in change_user_info_form.errors.values():
            for error in errors:
                messages.error(request, error)
    return redirect(request.META.get('HTTP_REFERER', 'SJLApp:home'))

@require_http_methods(["POST"])
@login_required(login_url=reverse_lazy('login_page'))
def change_password(request):
    change_pass_form = ChangePasswordForm(user = request.user, data=request.POST)
    if change_pass_form.is_valid():
        password = change_pass_form.cleaned_data.get('password')
        request.user.set_password(password)
        request.user.save()
    else:
        for errors in change_pass_form.errors.values():
            for error in errors:
                messages.error(request, error)
    return redirect(request.META.get('HTTP_REFERER', 'SJLApp:home'))

@require_http_methods(["POST"])
@login_required(login_url=reverse_lazy('login_page'))
def change_about(request):
    about_form = AboutForm(request.POST)
    if about_form.is_valid():
        new_about = about_form.cleaned_data.get('about')
        request.user.profile.about = new_about
        request.user.save()
    else:
        for errors in about_form.errors.values():
            for error in errors:
                messages.error(request, error)
    return redirect(request.META.get('HTTP_REFERER', 'SJLApp:home'))

@require_http_methods(["POST"])
def user_login(request):
    login_form = LoginForm(request.POST)
    if login_form.is_valid():
        username = login_form.cleaned_data['username']
        password = login_form.cleaned_data['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
    else:
        for errors in login_form.errors.values():
            for error in errors:
                messages.error(request, error)
    return redirect('SJLApp:user')

@require_http_methods(["POST"])
@login_required(login_url=reverse_lazy('SJLApp:login_page'))
def user_logout(request):
    logout(request)
    return redirect(request.META.get('HTTP_REFERER', 'SJLApp:home'))