from django.urls import reverse
from django.contrib.auth.models import User
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from SJLApp.models import *

class CreateUser(LiveServerTestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
        }
        self.selenium = webdriver.Chrome()
        self.wait = WebDriverWait(self.selenium, 10)

    def test_create_nav(self):
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@data-bs-target='#offcanvasNavbar']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='offcanvasNavbar']//a[contains(text(), 'Create Account')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:create_user_page'))
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='email']"))).send_keys("example@email.com")
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='username']"))).send_keys(self.credentials["username"])
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='password']"))).send_keys(self.credentials["password"])
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='confirm_password']"))).send_keys(self.credentials["password"])
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Submit')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:login_page'))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Close')]"))).click()
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='username']"))).send_keys(self.credentials["username"])
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='password']"))).send_keys(self.credentials["password"])
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Submit')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:user'))

    def test_create_dropdown(self):
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@data-bs-toggle='dropdown']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class, 'dropdown-menu')]//a[contains(text(), 'Dont have an account?')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+'/account/create-page/')
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='email']"))).send_keys("example@email.com")
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='username']"))).send_keys(self.credentials["username"])
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='password']"))).send_keys(self.credentials["password"])
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='confirm_password']"))).send_keys(self.credentials["password"])
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Submit')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:login_page'))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Close')]"))).click()
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='username']"))).send_keys(self.credentials["username"])
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='password']"))).send_keys(self.credentials["password"])
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Submit')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:user'))

class ViewUser(LiveServerTestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
        }
        self.user = User.objects.create_user(**self.credentials)
        self.shaders = {}
        for i in range(10):
            self.shaders[i] = Shader(title=f"Title{i}", user=self.user)
            self.shaders[i].save()
        self.selenium = webdriver.Chrome()
        self.wait = WebDriverWait(self.selenium, 10)

    def test_index_search(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:user'))
        assert 'Title1' in self.selenium.page_source
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@id='search-input']"))).send_keys("9")
        self.selenium.execute_script('arguments[0].click()',self.selenium.find_elements_by_xpath("//button[@id='search-btn']")[0])
        assert 'Title9' in self.selenium.page_source
        assert not 'Title1' in self.selenium.page_source

    def test_index_pages(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:user'))
        assert 'Title1' in self.selenium.page_source
        self.selenium.execute_script('arguments[0].click()',self.selenium.find_elements_by_xpath("//button[contains(@class, 'next-btn')]")[0])
        assert 'Title9' in self.selenium.page_source
        self.selenium.execute_script('arguments[0].click()',self.selenium.find_elements_by_xpath("//button[contains(@class, 'prev-btn')]")[0])
        assert 'Title1' in self.selenium.page_source

    def test_viewer(self):
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk':self.shaders[0].id}))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[contains(text(), 'username')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:user_detail_page', kwargs={'pk':self.user.id}))
        assert 'Account Settings' not in self.selenium.page_source

    def test_owner(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.execute_script('arguments[0].click()', self.selenium.find_elements_by_xpath("//a[contains(@class, 'shader-card')]")[0])
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:shader_detail_page', kwargs={'pk':self.shaders[0].id}))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@id='menu-button-right']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='info']//a[contains(text(), 'username')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:user_detail_page', kwargs={'pk':self.user.id}))
        assert 'Account Settings' in self.selenium.page_source

class UpdateUser(LiveServerTestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
        }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()
        self.selenium = webdriver.Chrome()
        self.wait = WebDriverWait(self.selenium, 10)

    def test_info(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@data-bs-toggle='dropdown']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class, 'dropdown-menu')]//a[contains(text(), 'My Profile')]"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Account Settings')]"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[contains(text(), 'Edit My Information')]"))).click()
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='first_name']"))).send_keys("First")
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='last_name']"))).send_keys("Last")
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='email']"))).send_keys("email@email.com")
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='editInformation']//button[contains(text(), 'Submit')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:user'))
        assert self.selenium.find_element_by_xpath("//input[@name='first_name']").get_attribute("value") == "First"
        assert self.selenium.find_element_by_xpath("//input[@name='last_name']").get_attribute("value") == "Last"
        assert self.selenium.find_element_by_xpath("//input[@name='email']").get_attribute("value") == "email@email.com"

    def test_password(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@data-bs-toggle='dropdown']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class, 'dropdown-menu')]//a[contains(text(), 'My Profile')]"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Account Settings')]"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[contains(text(), 'Change Password')]"))).click()
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='current_password']"))).send_keys(self.credentials["password"])
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='password']"))).send_keys("password1")
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//input[@name='confirm_password']"))).send_keys("password1")
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='changePassword']//button[contains(text(), 'Submit')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:login_page'))
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='username']"))).send_keys(self.credentials["username"])
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='container']//input[@name='password']"))).send_keys("password1")
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Submit')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:user'))

    def test_about(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@data-bs-toggle='dropdown']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class, 'dropdown-menu')]//a[contains(text(), 'My Profile')]"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'About Me')]"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Edit')]"))).click()
        self.wait.until(EC.visibility_of_element_located((By.XPATH, "//textarea[@name='about']"))).send_keys("New About Me Paragraph")
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='aboutShow']//button[contains(text(), 'Submit')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:user'))
        assert "New About Me Paragraph" in self.selenium.page_source

class LoginUser(LiveServerTestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
        }
        self.user = User.objects.create_user(**self.credentials)
        self.selenium = webdriver.Chrome()
        self.wait = WebDriverWait(self.selenium, 10)

    def test_login(self):
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@data-bs-toggle='dropdown']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class, 'dropdown-menu')]//input[@name='username']"))).send_keys(self.credentials['username'])
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class, 'dropdown-menu')]//input[@name='password']"))).send_keys(self.credentials['password'])
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class, 'dropdown-menu')]//button[contains(text(), 'Login')]"))).click()
        assert self.selenium.current_url.__contains__(self.live_server_url+reverse('SJLApp:user'))

class LogoutUser(LiveServerTestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
        }
        self.user = User.objects.create_user(**self.credentials)
        self.selenium = webdriver.Chrome()
        self.wait = WebDriverWait(self.selenium, 10)

    def test_logout(self):
        self.client.login(**self.credentials)
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()
        self.selenium.get(self.live_server_url+reverse('SJLApp:index'))
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@data-bs-toggle='dropdown']"))).click()
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class, 'dropdown-menu')]//button[contains(text(), 'Logout')]"))).click()
        assert "Sign In" in self.selenium.page_source