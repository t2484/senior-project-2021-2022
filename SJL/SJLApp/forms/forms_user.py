from django import forms
from django.contrib.auth.models import User
from SJLApp.models import Profile

class LoginForm(forms.Form):
    username = forms.CharField(
        label='Username', max_length=32, min_length=1, required=True,
        widget=forms.TextInput(attrs={'placeholder': 'Username', 'class': 'form-control',}),
    )
    password = forms.CharField(
        label='Password', max_length=32, min_length=1, required=True,
        widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control',}),
    )

    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError('Username does not exist.')
        elif not User.objects.get(username=username).check_password(password):
            raise forms.ValidationError('Password is incorrect.')


class UserCreateForm(forms.Form):
    username = forms.CharField(
        label='Username', max_length=32, min_length=3, required=True,
        widget=forms.TextInput(attrs={'placeholder': 'Username', 'class': 'form-control',}), 
    )
    password = forms.CharField(
        label='Password', max_length=32, min_length=8, required=True,
        widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control',}),
    )
    confirm_password = forms.CharField(
        label='Confirm Password', max_length=32, min_length=8, required=True,
        widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password', 'class': 'form-control',}),
    )
    email = forms.CharField(
        label='Email', max_length=100, min_length=3, required=True,
        widget=forms.EmailInput(attrs={'placeholder': 'Email', 'class': 'form-control',}),
    )

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if User.objects.filter(username = username).exists():
            raise forms.ValidationError('Username already in use')
        return username

    def clean_confirm_password(self):
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if password != confirm_password:
            raise forms.ValidationError('Passwords don\'t match')
        return password

class ChangePasswordForm(forms.Form):
    current_password = forms.CharField(
        label='Current Password', max_length=32, min_length=1, required=True,
        widget=forms.PasswordInput(attrs={'placeholder': 'Current Password', 'class': 'form-control',}), 
    )
    password = forms.CharField(
        label='Password', max_length=32, min_length=8, required=True,
        widget=forms.PasswordInput(attrs={'placeholder': 'Password','class': 'form-control',}),
    )
    confirm_password = forms.CharField(
        label='Confirm Password', max_length=32, min_length=8, required=True,
        widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password', 'class': 'form-control',}),
    )

    def __init__(self, user, data=None):
        self.user = user
        super(ChangePasswordForm, self).__init__(data=data)

    def clean_current_password(self):
        current_password = self.cleaned_data.get('current_password', None)
        if not self.user.check_password(current_password):
            raise forms.ValidationError('Invalid current password')
        return current_password

    def clean_confirm_password(self):
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if password != confirm_password:
            raise forms.ValidationError('New passwords don\'t match')
        return confirm_password

class AboutForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['about']
        widgets = {
            'about': forms.Textarea(attrs={
                'placeholder': 'Write about yourself', 
                'class': 'form-control',
            }),
        }

class ChangeUserInfoForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']
        widgets = {
            'first_name': forms.TextInput(attrs={
                'placeholder': 'First Name',
                'class': 'form-control',
                }),
            'last_name': forms.TextInput(attrs={
                'placeholder': 'Last Name',
                'class': 'form-control',
                }),
            'email': forms.TextInput(attrs={
                'placeholder': 'Email',
                'class': 'form-control',
                }),
        }