from django.test import TestCase
from ..forms import *
from django.contrib.auth.models import User

# Create your tests here.
class TestUserCreation(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username = 'usedusername', password = 'usedpassword')
        self.form_data = {'username': 'username', 'password': 'password', 'confirm_password': 'password', 'email': 'newemail@example.com'}

    def test_normal_create(self):
        form = UserCreateForm(data = self.form_data)
        self.assertTrue(form.is_valid())

    def test_used_username(self):
        form_data = self.form_data.copy()
        form_data['username'] = self.user.username
        form = UserCreateForm(data = form_data)
        self.assertFalse(form.is_valid())

    def test_short_username(self):
        form_data = self.form_data.copy()
        form_data['username'] = 'u'
        form = UserCreateForm(data = form_data)
        self.assertFalse(form.is_valid())

    def test_long_username(self):
        form_data = self.form_data.copy()
        form_data['username'] = 'usernameusernameusernameusernameusernameusername'
        form = UserCreateForm(data = form_data)
        self.assertFalse(form.is_valid())

    def test_short_password(self):
        form_data = self.form_data.copy()
        form_data['password'] = 'passwor'
        form_data['confirm_password'] = 'passwor'
        form = UserCreateForm(data = form_data)
        self.assertFalse(form.is_valid())

    def test_long_password(self):
        form_data = self.form_data.copy()
        form_data['password'] = 'passwordpasswordpasswordpasswordpasswordpassword'
        form_data['confirm_password'] = 'passwordpasswordpasswordpasswordpasswordpassword'
        form = UserCreateForm(data = form_data)
        self.assertFalse(form.is_valid())

    def test_unequal_passwords(self):
        form_data = self.form_data.copy()
        form_data['password'] = 'passwordx'
        form_data['confirm_password'] = 'passwordy'
        form = UserCreateForm(data = form_data)
        self.assertFalse(form.is_valid())

class TestUserLogin(TestCase):
    def setUp(self):
        User.objects.create_user(username = 'username', password = 'password')
        self.form_data = {'username': 'username', 'password': 'password'}

    def test_normal_login(self):
        form = LoginForm(data = self.form_data)
        self.assertTrue(form.is_valid())

    def test_nonexistant_username(self):
        form_data = self.form_data.copy()
        form_data['username'] = 'xusernamex'
        form = LoginForm(data = form_data)
        self.assertFalse(form.is_valid())

    def test_wrong_password(self):
        form_data = self.form_data.copy()
        form_data['password'] = 'xpasswordx'
        form = LoginForm(data = form_data)
        self.assertFalse(form.is_valid())

class TestChangePassword(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username = 'username', password = 'password')
        self.form_data = {'current_password': 'password', 'password': 'newpassword', 'confirm_password': 'newpassword'}

    def test_normal_change(self):
        form = ChangePasswordForm(user = self.user, data = self.form_data)
        self.assertTrue(form.is_valid())

    def test_wrong_confirm_pass(self):
        form_data = self.form_data.copy()
        form_data['confirm_password'] = 'xnewpasswordx'
        form = ChangePasswordForm(user = self.user, data = form_data)
        self.assertFalse(form.is_valid())

    def test_wrong_current_pass(self):
        form_data = self.form_data.copy()
        form_data['current_password'] = 'xpasswordx'
        form = ChangePasswordForm(user = self.user, data = form_data)
        self.assertFalse(form.is_valid())

class TestChangeUserInfo(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username = 'username', password = 'password')

    def test_change_first_name(self):
        form_data = {'first_name': 'newfirstname'}
        form = ChangeUserInfoForm(data = form_data)
        self.assertTrue(form.is_valid())

    def test_change_last_name(self):
        form_data = {'last_name': 'newlastname'}
        form = ChangeUserInfoForm(data = form_data)
        self.assertTrue(form.is_valid())

    def test_change_email(self):
        form_data = {'email': 'newemail@example.com'}
        form = ChangeUserInfoForm(data = form_data)
        self.assertTrue(form.is_valid())

    def test_change_all(self):
        form_data = {'first_name': 'newfirstname', 'last_name': 'newlastname', 'email': 'newemail@example.com'}
        form = ChangeUserInfoForm(data = form_data)
        self.assertTrue(form.is_valid())

class TestAboutForm(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username = 'username', password = 'password')

    def test_normal(self):
        form_data = {'about': 'About Me'}
        form = AboutForm(data = form_data)
        self.assertTrue(form.is_valid)
