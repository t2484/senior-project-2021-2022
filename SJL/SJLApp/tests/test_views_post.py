from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.auth import get_user
from django.urls import reverse
from SJLApp.views import *
from SJLApp.models import *

class TestCreateShader(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.client.login(**self.credentials)

    def test_create_shader(self):
        response = self.client.post(reverse('SJLApp:create_shader'))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Shader.objects.all().exists())

class TestUpdateShader(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.data = {
            'title': 'title',
            'description': 'description',
            'is_public': True,
            'code': 'code',
            'config': '{"a": "b"}',
        }
        self.shader = Shader(**self.data)
        self.shader.user = self.user
        self.shader.save()
        self.client.login(**self.credentials)

    def test_update_shader(self):
        self.data['title'] = 'newtitle'
        response = self.client.post(reverse('SJLApp:update_shader', kwargs={'pk':self.shader.pk}), self.data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Shader.objects.get(pk=self.shader.id).title, self.data['title'])

    def test_update_shader_bad(self):
        self.data['title'] = 'newtitle'
        self.data.pop('config')
        response = self.client.post(reverse('SJLApp:update_shader', kwargs={'pk':self.shader.pk}), self.data)
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(Shader.objects.get(pk=self.shader.id).title, self.data['title'])

class TestDeleteShader(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()
        self.client.login(**self.credentials)

    def test_delete_shader(self):
        response = self.client.post(reverse('SJLApp:delete_shader', kwargs={'pk':self.shader.pk}))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Shader.objects.all().exists())

class TestCreateComment(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()
        self.client.login(**self.credentials)

    def test_create_comment(self):
        response = self.client.post(reverse('SJLApp:comment', kwargs={'pk':self.shader.pk}))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Comment.objects.all().exists())

    def test_create_bad(self):
        data = {
            'content': 'x'*257
        }
        response = self.client.post(reverse('SJLApp:comment', kwargs={'pk':self.shader.pk}), data)
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Comment.objects.all().exists())

class TestDeleteComment(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.shader = Shader()
        self.shader.user = self.user
        self.shader.save()
        self.comment = Comment()
        self.comment.shader = self.shader
        self.comment.user = self.user
        self.comment.save()
        self.client.login(**self.credentials)

    def test_create_comment(self):
        response = self.client.post(reverse('SJLApp:delete_comment', kwargs={'pk':self.comment.pk}))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Comment.objects.all().exists())

class TestCreateUser(TestCase):
    def setUp(self):
        self.credentials = {
            'email': 'example@example.com',
            'username': 'username',
            'password': 'password',
            'confirm_password': 'password'
            }

    def test_create_user(self):
        response = self.client.post(reverse('SJLApp:create_user'), self.credentials)
        self.assertTrue(User.objects.all().exists())
        self.assertEqual(response.status_code, 302)

    def test_create_user_bad(self):
        credentials = self.credentials.copy()
        credentials['confirm_password'] = 'wrongpassword'
        response = self.client.post(reverse('SJLApp:create_user'), credentials)
        self.assertFalse(User.objects.all().exists())
        self.assertEqual(response.status_code, 302)

class TestChangeUserInfo(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.client.login(**self.credentials)

    def test_change_user_info(self):
        data = {
            'first_name': 'newfirstname', 
            'last_name': 'newlastname', 
            'email': 'newemail@example.com',
            }
        response = self.client.post(reverse('SJLApp:change_user_info'), data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(get_user(self.client).username, self.user.username)
        self.assertEqual(get_user(self.client).first_name, data['first_name'])
        self.assertEqual(get_user(self.client).last_name, data['last_name'])
        self.assertEqual(get_user(self.client).email, data['email'])

    def test_change_user_info_bad(self):
        data = {
            'first_name': 'newfirstname', 
            'last_name': 'newlastname', 
            'email': 'bademail',
            }
        response = self.client.post(reverse('SJLApp:change_user_info'), data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(get_user(self.client).username, self.user.username)
        self.assertNotEqual(get_user(self.client).first_name, data['first_name'])
        self.assertNotEqual(get_user(self.client).last_name, data['last_name'])
        self.assertNotEqual(get_user(self.client).email, data['email'])

class TestChangePassword(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.client.login(**self.credentials)

    def test_password(self):
        data = {
            'current_password': self.credentials['password'],
            'password': 'newpassword',
            'confirm_password': 'newpassword',
        }
        response = self.client.post(reverse('SJLApp:change_pass'), data)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(User.objects.get(username=self.credentials['username']).check_password(data['password']))

    def test_password_bad(self):
        data = {
            'current_password': self.credentials['password'],
            'password': 'newpassword',
            'confirm_password': 'wrongpassword',
        }
        response = self.client.post(reverse('SJLApp:change_pass'), data)
        self.assertEqual(response.status_code, 302)
        self.assertFalse(User.objects.get(username=self.credentials['username']).check_password(data['password']))

class TestChangeAbout(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.client.login(**self.credentials)

    def test_change_about(self):
        data = {
            'about': 'About Me',
            }
        response = self.client.post(reverse('SJLApp:change_about'), data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(get_user(self.client).username, self.user.username)
        self.assertEqual(get_user(self.client).profile.about, data['about'])

    def test_change_about_bad(self):
        data = {
            'about': 'About Me'*257,
            }
        response = self.client.post(reverse('SJLApp:change_about'), data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(get_user(self.client).username, self.user.username)
        self.assertNotEqual(get_user(self.client).profile.about, data['about'])

class TestLogin(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)

    def test_login(self):
        response = self.client.post(reverse('SJLApp:login'), self.credentials)
        self.assertTrue(get_user(self.client).is_authenticated)
        self.assertEqual(response.status_code, 302)

    def test_bad_login(self):
        credentials = {
            'username': 'username',
            'password': 'wrongpassword'
            }
        response = self.client.post(reverse('SJLApp:login'), credentials)
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertEqual(response.status_code, 302)

class TestLogout(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'username',
            'password': 'password'
            }
        self.user = User.objects.create_user(**self.credentials)
        self.client.login(**self.credentials)

    def test_logout(self):
        response = self.client.post(reverse('SJLApp:logout'))
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertEqual(response.status_code, 302)