from django.urls import re_path
from . import views

app_name = 'SJLApp'

#Links view methods to urls
urlpatterns = [
    re_path(r'^$', views.index, name='home'),
    re_path(r'^about-us/$', views.about_us, name='about_us'),
    re_path(r'^index/$', views.index, name='index'),

    re_path(r'^shader/(?P<pk>\d+)/$', views.shader_detail, name='shader_detail_page'),

    re_path(r'^create-shader/$', views.create_shader, name='create_shader'),
    re_path(r'^update-shader/(?P<pk>\d+)/$', views.update_shader, name='update_shader'),
    re_path(r'^delete-shader/(?P<pk>\d+)/$', views.delete_shader, name='delete_shader'),

    re_path(r'^comment/(?P<pk>\d+)/$', views.create_comment, name='comment'),
    re_path(r'^delete-comment/(?P<pk>\d+)/$', views.delete_comment, name='delete_comment'),

    re_path(r'^user/$', views.user, name='user'),
    re_path(r'^user/(?P<pk>\d+)/$', views.any_user, name='user_detail_page'),

    re_path(r'^account/login-page/', views.user_login_page, name='login_page'),
    re_path(r'^account/login/$', views.user_login, name='login'),
    re_path(r'^account/logout/$', views.user_logout, name='logout'),
    re_path(r'^account/create-page/$', views.create_user_page, name='create_user_page'),
    re_path(r'^account/create/$', views.create_user, name='create_user'),
    re_path(r'^account/change-user-info/$', views.change_user_info, name='change_user_info'),
    re_path(r'^account/change-pass/$', views.change_password, name='change_pass'),
    re_path(r'account/change-about/$', views.change_about, name='change_about'),

    re_path(r'^api/shader/$', views.api_shaders, name='api_shaders'),
    re_path(r'^api/shader/(?P<pk>\d+)/$', views.api_shader, name='api_shader'),
    re_path(r'^api/user/(?P<pk>\d+)/$', views.api_user_shaders, name='api_user_shaders'),
]

handler404 = "SJLApp.views.error404"
